package com.events.events.Métier;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "participant")
public class Participant {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public int id;
	
	@Column(name = "nom", nullable = false)
	public String nom;
	
	@Column(name = "prenom", nullable = false)
	public String prenom;
	
	@Column(name = "email", nullable = false)
	public String email;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_naissance", nullable = false)
	public Date date_naissance;
	
	@ManyToOne(fetch = FetchType.LAZY, optional=false)
	public Evenement evenement;
	
	public Participant(String nom, String prenom, String email , Evenement evenment) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.date_naissance = new Date();
		this.evenement = evenment;
	}
	
	public Participant() {
		
	}
	
	@Override
	public String toString() {
		String result = prenom + " " + nom + " (" + email + ")";
		//for (Evenement evenement : evenements) {
		//	result += "\n - " + evenement; 
		//}
		return result;
	}

}