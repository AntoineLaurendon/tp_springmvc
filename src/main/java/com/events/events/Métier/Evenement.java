package com.events.events.Métier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "evenement")
public class Evenement {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public int id;
	
	@Column(name = "lieu", nullable = false)
	public String lieu;
	
	@Column(name = "nom", nullable = false)
	public String nom;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	public Date date;
	
	
	
	
	public Evenement(String nom, String lieu) {
		this.nom = nom;
		this.lieu = lieu;
		this.date = new Date();
	}

	public Evenement() {
	}
	

}
