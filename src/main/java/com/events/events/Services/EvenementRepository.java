package com.events.events.Services;

import org.springframework.data.repository.CrudRepository;

import com.events.events.Métier.Evenement;

public interface EvenementRepository extends CrudRepository<Evenement, Integer> {

}
