package com.events.events.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.events.events.Services.ParticipantRepository;
import com.events.events.Métier.Evenement;
import com.events.events.Métier.Participant;
import com.events.events.Services.EvenementRepository;

@Controller
public class ParticipantController {
	
	@Autowired
	private ParticipantRepository participantRepository;
	@Autowired
	private EvenementRepository evenementRepository;
	
	@GetMapping("/liste_participants")
	public String listeParticipants(Model model) {
		
		// Ajout de données au modèle
		model.addAttribute("listeParticipants", (List<Participant>) this.participantRepository.findAll());
		model.addAttribute("listeEvenements", (List<Evenement>) evenementRepository.findAll());

		return "liste_participants";
	}
	
	
    @GetMapping("/add_participant")
	public String addParticipant(Model model, @RequestParam String nom, @RequestParam String prenom, @RequestParam String email, @RequestParam String id_event) {
    	
    	Evenement event = evenementRepository.findById(Integer.parseInt(id_event)).get();
    	Participant user = new Participant(nom, prenom, email, event);
    	
    	participantRepository.save(user);
    	
    	model.addAttribute("listeParticipants", this.participantRepository.findAll());
    	model.addAttribute("listeEvenements", (List<Evenement>) evenementRepository.findAll());
    	
    	return "liste_participants";
    }
	
}