package com.events.events.Controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.events.events.Métier.Evenement;
import com.events.events.Services.EvenementRepository;

@Controller
public class EvenementController {
	
	@Autowired
	private EvenementRepository evenementRepository;
	
	@GetMapping("/liste_evenements")
	public String listeEvenements(Model model) {
		
		// Ajout de données au modèle
		System.out.println(evenementRepository.findAll().getClass().getName());
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		//Envoi vers la vue
		return "liste_evenements";
	}
	
	
	@GetMapping("/add_evenement")
	public String addEvenements(Model model, @RequestParam String event_name, @RequestParam String event_place) {
		
		Evenement event = new Evenement(event_name, event_place);	
		evenementRepository.save(event);
		
		// Ajout de données au modèle
		model.addAttribute("listeEvenements", (List<Evenement>) evenementRepository.findAll());
		List<Evenement> events = (List<Evenement>) evenementRepository.findAll();
			
		//Envoi vers la vue
		return "liste_evenements";
	}

	
}
