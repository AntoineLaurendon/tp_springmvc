# TP_SpringMVC

## Présentation
Rendu du TP Spring Boot réalisé lors du cours de J2E - Back-end.

Technologies utilisées : 
- Sprint Boot
- Thymeleaf
- Base de données PostGreSQL

## Cloner le projet
Lancer dans un terminal la commande suivante :
```
git clone https://gitlab.com/AntoineLaurendon/tp_springmvc
```

## Éxécution de projet
Après avoir cloné le projet : 
```
cd tp_springmvc
./mvnw spring-boot:run

```
